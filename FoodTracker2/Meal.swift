//
//  Meal.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 7/6/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import UIKit

class Meal: NSObject, NSCoding {
    // MARK: Properties
    
    struct PropertyKey {
        
        static let nameKey = "name"
        static let photoKey = "photo"
        static let ratingKey = "rating"
        static let isLikedKey = "isLiked"
        
    }
    
        var name : String
        var photo : UIImage?
        var rating : Int
        var isLiked : Bool
    
    static var mealsStaticCollection = [Meal]()
    
    // MARK: Archiving Paths
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("meals")

    
    // MARK: Init
    
    init?(name : String, photo : UIImage?, rating : Int, isLiked : Bool) { //failable init means possible to -> nil
        
        // Initialize stored properties.
        self.name = name
        self.photo = photo
        self.rating = rating
        self.isLiked = isLiked
        
        super.init()
        
        // Initialization should fail if there is no name or if the rating is negative.
        if name.isEmpty || rating < 0 {
            return nil
        }
    }
    
    
    // MARK: NSCoding
    
    // convenience're secondary, support initializers that need to call one of their class’s designated initializer
    required convenience init?(coder aDecoder: NSCoder) {
        
        // unarchives the stored information stored about an object
        let name = aDecoder.decodeObjectForKey(PropertyKey.nameKey) as! String
        
        // because photo is optional property of Meal, use conditional cast
        let photo = aDecoder.decodeObjectForKey(PropertyKey.photoKey) as? UIImage
        
        // method unarchives an integer. no need to downcast
        let rating = aDecoder.decodeIntegerForKey(PropertyKey.ratingKey)
        
        // method unarchives a bool. no need to downcast
        let isLiked = aDecoder.decodeBoolForKey(PropertyKey.isLikedKey)
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, rating: rating, isLiked : isLiked)
        
    }
    
    // method prepares the class’s information to be archived
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeObject(name, forKey: PropertyKey.nameKey)
        aCoder.encodeObject(photo, forKey: PropertyKey.photoKey)
        aCoder.encodeInteger(rating, forKey: PropertyKey.ratingKey)
        aCoder.encodeBool(isLiked, forKey: PropertyKey.isLikedKey)
    }
}
