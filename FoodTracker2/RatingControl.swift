//
//  RatingControl.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 6/30/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import UIKit

class RatingControl: UIView {
    
    // MARK: Properties
    
    var rating = 0 {
        // This ensures that the UI is showing an accurate representation of the rating
        didSet {
            setNeedsLayout() // trigger a layout update every time the rating changes
        }
    }
    
    var ratingButtons = [UIButton]()
    let spacing = 5
    let starCount = 5
    
    // MARK: Initialization
    
    //method for layouting buttons
    override func layoutSubviews() {
        
        // Set button's width and height to square size of frame's height.
        let buttonSize = Int(frame.size.height) / 2
        var buttonFrame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        
        // Offset each button's origin by the length of the button plus spacing.
        for (index, button) in ratingButtons.enumerate() {
            buttonFrame.origin.x = CGFloat(index * (buttonSize + spacing))
            button.frame = buttonFrame
        }
        
    updateButtonSelectionStates()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let fullAppleImage = UIImage(named: "fullApple")
        let emptyAppleImage = UIImage(named: "emptyApple")
        
        // add array of button
        for _ in 0 ..< starCount {
            let button = UIButton()
            
            // set button status selected/unselected/highlighted
            button.setImage(emptyAppleImage, forState: .Normal)
            button.setImage(fullAppleImage, forState: .Selected)
            button.setImage(fullAppleImage, forState: [.Highlighted, .Selected])
            
            // the image doesn’t show an additional highlight
            button.adjustsImageWhenHighlighted = false
            
            button.addTarget(self, action:Selector("ratingButtonTapped:"), forControlEvents: .TouchDown)
            ratingButtons += [button]
            addSubview(button)

        }
        func intrinsicContentSize() -> CGSize {
        // calculate the view control’s size
            let buttonSize = Int(frame.size.height) / 2
            let width = (buttonSize * starCount) + (spacing * (starCount - 1))
            
            return CGSize(width: width, height: buttonSize)
        }
        
    }
    
    // MARK: Button Action
    
    func ratingButtonTapped(button: UIButton) { // return index of a button
        rating = ratingButtons.indexOf(button)! + 1
        updateButtonSelectionStates()
    }
    
    func updateButtonSelectionStates() { // method for update selection state of buttons
        // if index < rating, that button should be selected
        for (index, button) in ratingButtons.enumerate() {
            button.selected = index < rating
        }
    }
}
