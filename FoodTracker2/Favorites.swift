//  Favorites.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 7/26/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.

import UIKit

class Favorites: UIView {
    
    // MARK: Property
    let likeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    var isLiked = false {
        didSet {
            likeButton.selected = isLiked
        }
    }
    
    var delegate: FavoriteControlDelegate?
    
    // MARK: Initialization
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        let likedImage = UIImage(named: "Liked")
        let disLikedImage = UIImage(named: "Disliked")
        
        likeButton.setImage(disLikedImage, forState: .Normal)
        likeButton.setImage(likedImage, forState: .Selected)
        
        likeButton.addTarget(self, action:Selector("likeButtonTapped:"), forControlEvents: .TouchDown)
        addSubview(likeButton)
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    // MARK: Favorite button tapped
    
    func likeButtonTapped(sender: UIButton) {
        isLiked = !isLiked
        sender.selected = isLiked
        delegate?.favoriteControl(self, changedStateTo: isLiked)
    }
}
