//
//  MealViewController.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 6/8/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import UIKit

class MealViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: Properties
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var isLikedControl: Favorites!
    
    
    /*
    This value is either passed by `MealTableViewController` in `prepareForSegue(_:sender:)`
    or constructed as part of adding a new meal.
    */
    var meal : Meal?
    
    var destianationController = destiantionType.defaultTVC
    
    enum destiantionType {
        case MTVC
        case FTVC
        case defaultTVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Now ViewController class is delegate of text field
        nameTextField.delegate = self
        
        // Set up views if editing an existing Meal.
        if let meal = meal {
            navigationItem.title = meal.name
            nameTextField.text   = meal.name
            photoImageView.image = meal.photo
            ratingControl.rating = meal.rating
            isLikedControl.isLiked = meal.isLiked
        }
        
        // Enable Save button only if nameTextField is valid
        checkValidMealName()
    }
    
    // MARK: UITextFieldDelegate
    
    // Disable the Save button while editing
    func textFieldDidBeginEditing(textField: UITextField) {
        saveButton.enabled = false
    }
    
    // Disable the Save button if the text field is empty
    func checkValidMealName() {
        let text = nameTextField.text ?? ""
        saveButton.enabled = !text.isEmpty        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // First responser is resigned after user taps Return, hide keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        checkValidMealName()
        navigationItem.title = textField.text
    }
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        // The info dictionary contains multiple representations of the image, and this uses the original.
        
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: Actions
    
    @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {
        
        nameTextField.resignFirstResponder()
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .PhotoLibrary
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    @IBAction func saveAction(sender: UIBarButtonItem) {
        
        let name = nameTextField.text ?? "" // ?? is used to -> the value of optional or default value
        let photo = photoImageView.image
        let rating = ratingControl.rating
        let isLiked = isLikedControl.isLiked
        
        // Set the meal to be passed to MealTableViewController after the unwind segue
        meal = Meal(name: name, photo: photo, rating: rating, isLiked: isLiked)
        
        // destination becomes an object of MTVC/FTVC by getting to stack of navigation controllers
        
        switch destianationController {
            
            case .MTVC :
                let destination = navigationController!.viewControllers[navigationController!.viewControllers.count - 2] as! MealTableViewController
                destination.unwindToMealList(self)
            
            case .FTVC :
                let destination = navigationController!.viewControllers[navigationController!.viewControllers.count - 2] as! FavoritesTableViewController
                destination.unwindToFavoritesList(self)
            
            case .defaultTVC :
                print("switch failed")
    
        }
        
        navigationController!.popViewControllerAnimated(true)
    
    }
    
    
    // MARK: Navigation
    @IBAction func cancel(sender: UIBarButtonItem) {
        navigationController!.popViewControllerAnimated(true)
    }
}




