//
//  MealTableViewController.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 7/7/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import UIKit


class MealTableViewController: UITableViewController, UISearchBarDelegate, FavoriteControlDelegate {
    
    // MARK: Properties
    
    
    @IBOutlet weak var searchMeals: UISearchBar!
    
    var meals = [Meal]()
    var searchResults : [Meal]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchMeals.delegate = self
        
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem()
        
        // Load any saved meals
        if let savedMeals = loadMeals() {
            Meal.mealsStaticCollection = savedMeals
        } else {
            loadSampleMeals() // Load the sample data.
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        saveMeals()
        tableView.reloadData()
    }

    
    func loadSampleMeals() { // method to load sample data
    
        let photo1 = UIImage(named: "Image1")!
        let meal1 = Meal(name: "Caprese Salad", photo: photo1, rating: 4, isLiked: true)!
        
        let photo2 = UIImage(named: "Image2")!
        let meal2 = Meal(name: "Chicken and Potatoes", photo: photo2, rating: 5, isLiked: false)!
        
        let photo3 = UIImage(named: "Image3")!
        let meal3 = Meal(name: "Pasta with Meatballs", photo: photo3, rating: 3, isLiked: false)!
        
        Meal.mealsStaticCollection += [meal1, meal2, meal3]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // return number of needed sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if searchResults != nil {
            return searchResults!.count
            
        } else {
            // return the number of rows needed for table
            return Meal.mealsStaticCollection.count
        }
    
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "MealTableViewCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! MealTableViewCell
        
        var meal : Meal
        if searchResults != nil {
            
            meal = searchResults![indexPath.row]
        } else {
            meal = Meal.mealsStaticCollection[indexPath.row] // fetches the appropriate meal for the data source layout
        }

        cell.nameLabel.text = meal.name
        cell.photoImageView.image = meal.photo
        cell.ratingControl.rating = meal.rating
        cell.isLikedControl.isLiked = meal.isLiked
        cell.isLikedControl.delegate = self
        
        return cell
    }
    
    
    func unwindToMealList(sender: MealViewController) {
       
        if let meal = sender.meal {
            
            // whether a row in the table view is selected. If so -> user tapped one of table cells
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                
                // updates appropriate entry in meals to store updated meal information
                Meal.mealsStaticCollection[selectedIndexPath.row] = meal
                
                // reloads appropriate row in table view to display changed data
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
                
            } else {
            
                // Add a new meal
                let newIndexPath = NSIndexPath(forRow: Meal.mealsStaticCollection.count, inSection: 0)
                Meal.mealsStaticCollection.append(meal)
            
                // animates addition of a new row
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
            
            // Save the meals
            saveMeals() // here is a global change to hard drive storage, that's why saveMeals()
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
       (segue.destinationViewController as! MealViewController).destianationController = .MTVC
        
        if segue.identifier == "ShowDetail" {
            
        //downcast destination view controller of segue to MealViewController
            let mealDetailViewController = segue.destinationViewController as! MealViewController
            
            // Get cell that generated this segue.
            if let selectedMealCell = sender as? MealTableViewCell {
            
                let indexPath = tableView.indexPathForCell(selectedMealCell)!
                let selectedMeal = Meal.mealsStaticCollection[indexPath.row]
                mealDetailViewController.meal = selectedMeal
                
            }
        }
            
        else if segue.identifier == "AddItem" {
            let _ = segue.destinationViewController as! MealViewController
            print("Adding new meal.")
        }
    }
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            // Delete the row from the data source
            Meal.mealsStaticCollection.removeAtIndex(indexPath.row)
            saveMeals() // here is a global change to hard drive storage, that's why saveMeals()
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // MARK: NSCoding
    
    func saveStaticCollection(bufferObject : Meal, index : Int) {
        Meal.mealsStaticCollection[index] = bufferObject
    }
    
    func saveMeals() {
    // NSKeyedArchiver attempts to archive meals array to specific location, and -> true if success
    // Meal.ArchiveURL is let that is defined in Meal class to identify where to save the information
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(Meal.mealsStaticCollection, toFile: Meal.ArchiveURL.path!)
        
        if !isSuccessfulSave {
            print("Failed to save meals...")
        }
    }
    
    func loadMeals() -> [Meal]? {
    // attempts to unarchive object stored at Meal.ArchiveURL.path! and downcast to array of Meal
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Meal.ArchiveURL.path!) as? [Meal]
    }
    

    // MARK: SearchBar Delegate

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchResults = Meal.mealsStaticCollection.filter {
            searchText.isEmpty || ($0.name as NSString).localizedCaseInsensitiveContainsString("\(searchText)")
        }
        
        tableView.reloadData() // MTVC don't know that data is reload so we should tell MTVC to do it
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        searchResults = nil
        tableView.reloadData()
        searchBar.text = nil
    }

    // MARK: FavoriteControlDelegate
    
    func favoriteControl(control: Favorites, changedStateTo state: Bool) {

        let cell = tableView.visibleCells
                            .map { $0 as! MealTableViewCell }
                            .filter { $0.isLikedControl === control }
                            .first!

        
        let meal = Meal.mealsStaticCollection[tableView.indexPathForCell(cell)!.row]
        
        var indexInStaticCollection = 0
        
        for i in 0 ..< Meal.mealsStaticCollection.count { // here index in static collection is defined
            if meal.name == Meal.mealsStaticCollection[i].name {
                indexInStaticCollection = i
            }
        }
       
        meal.isLiked = state
        saveStaticCollection(meal, index: indexInStaticCollection) // here only property has been changed that's why saveStaticCollection() is used
        

    }

}