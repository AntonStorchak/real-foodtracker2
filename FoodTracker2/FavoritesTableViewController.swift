//
//  FavoritesTableViewController.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 7/29/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//


import UIKit

class FavoritesTableViewController: UITableViewController, FavoriteControlDelegate {

    
    // MARK: Properties
        
    var isLikedMeals = [Meal]()
    var staticIndex = 0
    static var objectThatIsGoingToBeChanged = FavoritesTableViewController()

    // MARK: NSCoding
    
    func saveMeals() {
        // NSKeyedArchiver attempts to archive meals array to specific location, and -> true if success
        // Meal.ArchiveURL is let that is defined in Meal class to identify where to save the information
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(isLikedMeals, toFile: Meal.ArchiveURL.path!)
        
        if !isSuccessfulSave {
            print("Failed to save meals...")
        }
    }
    
    func loadMeals() -> [Meal]? {
        // attempts to unarchive object stored at Meal.ArchiveURL.path! and downcast to array of Meal
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Meal.ArchiveURL.path!) as? [Meal]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {

        isLikedMeals = Meal.mealsStaticCollection.filter { $0.isLiked }
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    // MARK: Table view data source
        
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // return number of needed sections
        return 1
    }
        
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows needed for table
        return isLikedMeals.count
    }
        
        
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "FavoritesTableViewCell"

        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! FavoritesTableViewCell
            
        let meal = isLikedMeals[indexPath.row] // fetches the appropriate meal for the data source layout
        cell.nameLabel.text = meal.name
        cell.photoImageView.image = meal.photo
        cell.ratingControl.rating = meal.rating
        cell.isLikedControl.isLiked = meal.isLiked
        cell.isLikedControl.delegate = self
            
        return cell
    }
    
    func unwindToFavoritesList(sender: MealViewController) {
        
        let meal = sender.meal
            
        // whether a row in the table view is selected. If so -> user tapped one of table cells
        let selectedIndexPath = tableView.indexPathForSelectedRow!
                
        // updates appropriate entry in meals to store updated meal information
        isLikedMeals[selectedIndexPath.row] = meal!
                
        // reloads appropriate row in table view to display changed data
        tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            
        // here only property has been changed that's why saveStaticCollection() is used
        saveStaticCollection(meal!, index: FavoritesTableViewController.objectThatIsGoingToBeChanged.staticIndex)
    }

        
    // MARK: Navigation
    
    func saveStaticCollection(bufferObject : Meal, index : Int) {
        Meal.mealsStaticCollection[index] = bufferObject
    }
    
    func defineIndexInStaticCollection(meal : Meal) -> Int {
            
        var indexInStaticCollection = 0
            
        for i in 0 ..< Meal.mealsStaticCollection.count { // here index in static collection has been found
            if meal.name == Meal.mealsStaticCollection[i].name {
                indexInStaticCollection = i
                print(indexInStaticCollection)
            }
        }

        return indexInStaticCollection
    }
        
        
    //method for a little preparation before navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            
        (segue.destinationViewController as! MealViewController).destianationController = .FTVC
            
        if segue.identifier == "ShowFavoritesDetail" {
            
            //downcast destination view controller of segue to MealViewController
            let mealDetailViewController = segue.destinationViewController as! MealViewController
                
            // Get cell that generated this segue.
            if let selectedMealCell = sender as? FavoritesTableViewCell {
                    
                let indexPath = tableView.indexPathForCell(selectedMealCell)!
                let selectedMeal = isLikedMeals[indexPath.row]
                mealDetailViewController.meal = selectedMeal
                    
                FavoritesTableViewController.objectThatIsGoingToBeChanged.staticIndex = defineIndexInStaticCollection(selectedMeal)
                    
            }
        }
    }
    
    
    
    // MARK : FavoritesControlDelegate
    
    func favoriteControl(control: Favorites, changedStateTo state: Bool) {
        
        let cell = tableView.visibleCells
                            .map { $0 as! FavoritesTableViewCell }
                            .filter { $0.isLikedControl === control }
                            .first!

        let meal = isLikedMeals[tableView.indexPathForCell(cell)!.row]
        
        meal.isLiked = state
        
        // here only property has been changed that's why saveStaticCollection() is used
        saveStaticCollection(meal, index : defineIndexInStaticCollection(meal))
        
        isLikedMeals.removeAtIndex(tableView.indexPathForCell(cell)!.row)
        tableView.deleteRowsAtIndexPaths([tableView.indexPathForCell(cell)!], withRowAnimation: .Fade)
        
    }
}