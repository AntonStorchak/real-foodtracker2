//
//  FavoritesViewCell.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 8/3/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import UIKit

class FavoritesViewCell: MealTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
