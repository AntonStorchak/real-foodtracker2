//
//  FavoriteControlDelegate.swift
//  FoodTracker2
//
//  Created by Anton Storchak on 8/8/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import Foundation

protocol FavoriteControlDelegate {
    
    func favoriteControl(control: Favorites, changedStateTo state: Bool)
    
}