//
//  FoodTracker2Tests.swift
//  FoodTracker2Tests
//
//  Created by Anton Storchak on 6/8/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import XCTest
@testable import FoodTracker2

class FoodTracker2Tests: XCTestCase {
    
    // MARK: FoodTracker Tests
    
    // Test for Meal initializer -> when name.isEmpty || rating < 0
    func testMealInitialization() {
        
        // Success init
        let potentialItem = Meal(name: "Newest meal", photo: nil, rating: 5)
        XCTAssertNotNil(potentialItem) // XCTAssertNotNil tests Meal != nil
        
        // Failure init
        let noName = Meal(name: "", photo: nil, rating: 0)
        XCTAssertNil(noName, "Empty name is invalid") // XCTAssertNil asserts noName = nil
        
/*      Unit test should fail because rating < 0 => init should fail the check
      let badRating = Meal(name: "Really bad rating", photo: nil, rating: -1)
      XCTAssertNotNil(badRating) // This time XCTAsertNotNil'll show Meal = nil
*/        
        // Unit test should passed becase XCTAssertNil asserts Meal = nil
        let badRating = Meal(name: "Really bad rating", photo: nil, rating: -1)
        XCTAssertNil(badRating, "Negative ratings are invalid, be positive")
        
        
    }
}
